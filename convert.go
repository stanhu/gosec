package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gosec/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
	cweinfo "gitlab.com/gitlab-org/security-products/cwe-info-go"
)

// This tool was previously named GO_AST_SCANNER.
// backward compatibility
const legacyEnvVarConfidenceLevel = "SAST_GO_AST_SCANNER_LEVEL"
const envVarConfidenceLevel = "SAST_GOSEC_LEVEL"
const rawSourceCodeExtractMaxLength = 200

func convert(reader io.Reader, prependPath string) (*report.Report, error) {
	var doc = struct {
		Issues []Issue
	}{}

	err := json.NewDecoder(reader).Decode(&doc)
	if err != nil {
		return nil, err
	}

	minLevel := minConfidenceLevel() // TODO: extract level from cli context

	vulns := []report.Vulnerability{}
	for _, w := range doc.Issues {
		r := Result{w, prependPath}
		cwe, err := cweinfo.GetCweInfo(r.CWE.ID)

		var title = ""
		var description = ""

		if err == nil {
			title = cwe.Title
			description = cwe.Description
		} else {
			// Use old RuleID as fallback as a precaution. This should not be necessary as all the gosec rules are
			// mapped to CWEs
			title = fmt.Sprintf("Gosec Rule %s", r.RuleID)
			description = r.Details
		}

		if w.ConfidenceLevel() >= minLevel {
			vulns = append(vulns, report.Vulnerability{
				Category:             metadata.Type,
				Scanner:              metadata.IssueScanner,
				Message:              r.Details,
				Severity:             SeverityLevel(r.Severity),
				Confidence:           ConfidenceLevel(r.Confidence),
				CompareKey:           r.CompareKey(),
				Location:             r.Location(),
				Identifiers:          r.Identifiers(),
				Name:                 title,
				Description:          description,
				RawSourceCodeExtract: r.RawSourceCodeExtract(),
			})
		}
	}

	report := report.NewReport()
	report.Analyzer = metadata.AnalyzerID
	report.Config.Path = ruleset.PathSAST
	report.Vulnerabilities = vulns
	return &report, nil
}

func minConfidenceLevel() int {
	var value string
	if value = os.Getenv(envVarConfidenceLevel); value == "" {
		if value = os.Getenv(legacyEnvVarConfidenceLevel); value == "" {
			return 0
		}
	}
	level, err := strconv.Atoi(value)
	if err != nil || level > 3 || level < 0 {
		return 0
	}
	return level
}

// Result describes a result with a relative path.
type Result struct {
	Issue
	PrependPath string
}

// Filepath returns the relative path of the affected file.
func (r Result) Filepath() string {
	rel := strings.TrimPrefix(r.File, pathGoPkg)
	return filepath.Join(r.PrependPath, rel)
}

// CompareKey returns a string used to establish whether two issues are the same.
func (r Result) CompareKey() string {
	_, err := cweinfo.GetCweInfo(r.CWE.ID)
	var ruleID = ""
	if err == nil {
		ruleID = fmt.Sprintf("CWE-%s", r.CWE.ID)
	} else {
		// Use old RuleID as fallback as a precaution. This should not be necessary as all the gosec rules are
		// mapped to CWEs
		ruleID = r.RuleID
	}
	return strings.Join([]string{r.Filepath(), r.Line, r.Code, ruleID}, ":")
}

// Location returns a structured Location
func (r Result) Location() report.Location {
	location := report.Location{
		File: r.Filepath(),
	}
	if line, err := strconv.Atoi(r.Line); err != nil {
		lineRange := strings.Split(r.Line, "-")
		if len(lineRange) == 2 {
			startLine, _ := strconv.Atoi(lineRange[0])
			endLine, _ := strconv.Atoi(lineRange[1])
			location.LineStart = startLine
			location.LineEnd = endLine
		}
	} else {
		location.LineStart = line
	}
	return location
}

// Issue describes a vulnerability found in the source code.
type Issue struct {
	Severity   string  `json:"severity"`
	Confidence string  `json:"confidence"`
	RuleID     string  `json:"rule_id"`
	Details    string  `json:"details"`
	File       string  `json:"file"`
	Code       string  `json:"code"`
	Line       string  `json:"line"`
	CWE        CweInfo `json:"cwe"`
}

// CweInfo describes cwe information
type CweInfo struct {
	ID  string
	URL string
}

// ConfidenceLevel turns the confidence into an integer so that it can be compared.
func (i Issue) ConfidenceLevel() int {
	switch i.Confidence {
	case "LOW":
		return 1
	case "MEDIUM":
		return 2
	case "HIGH":
		return 3
	default:
		return 0
	}
}

// ConfidenceLevel returns the normalized severity.
// Gosec provides same values for both severity and confidence.
// See https://github.com/securego/gosec/blob/893b87b34342eadd448aba7638c5cc25f7ad26dd/issue.go#L63-L73
func ConfidenceLevel(s string) report.ConfidenceLevel {
	switch s {
	case "HIGH":
		return report.ConfidenceLevelHigh
	case "MEDIUM":
		return report.ConfidenceLevelMedium
	case "LOW":
		return report.ConfidenceLevelLow
	}
	return report.ConfidenceLevelUnknown
}

// SeverityLevel returns the normalized severity.
// Gosec provides same values for both severity and confidence.
// See https://github.com/securego/gosec/blob/893b87b34342eadd448aba7638c5cc25f7ad26dd/issue.go#L63-L73
func SeverityLevel(s string) report.SeverityLevel {
	switch s {
	case "HIGH":
		return report.SeverityLevelHigh
	case "MEDIUM":
		return report.SeverityLevelMedium
	case "LOW":
		return report.SeverityLevelLow
	}
	return report.SeverityLevelUnknown
}

// Identifiers returns a list of identifiers.
func (r Result) Identifiers() []report.Identifier {
	var identifiers []report.Identifier

	gosecIdentifier := report.Identifier{
		Type:  "gosec_rule_id",
		Name:  fmt.Sprintf("Gosec Rule ID %s", r.RuleID),
		Value: r.RuleID,
	}

	if url, ok := urls[r.RuleID]; ok {
		gosecIdentifier.URL = url
	}

	identifiers = append(identifiers, gosecIdentifier)

	cwe, err := cweinfo.GetCweInfo(r.Issue.CWE.ID)
	if err == nil {
		identifiers = append(identifiers, report.Identifier{
			Type:  "CWE",
			Name:  fmt.Sprintf("CWE-%s", cwe.ID),
			Value: cwe.ID,
			URL:   fmt.Sprintf("https://cwe.mitre.org/data/definitions/%s.html", cwe.ID),
		})
	} else if r.Issue.CWE.ID != "" {
		log.Warnf("Could not find cwe info for %s\n", r.Issue.CWE.ID)
	}

	return identifiers
}

// RawSourceCodeExtract returns an extract of the affected sourcecode
func (r Result) RawSourceCodeExtract() string {
	if len(r.Code) > rawSourceCodeExtractMaxLength {
		return r.Code[:rawSourceCodeExtractMaxLength]
	}
	return r.Code
}

// URLs for select rules
// Mapping to be replaced with https://github.com/securego/gosec/issues/127
var urls = map[string]string{
	"G101": "https://securego.io/docs/rules/g101.html",
	"G102": "https://securego.io/docs/rules/g102.html",
	"G103": "https://securego.io/docs/rules/g103.html",
	"G104": "https://securego.io/docs/rules/g104.html",
	"G107": "https://securego.io/docs/rules/g107.html",
	"G201": "https://securego.io/docs/rules/g201-g202.html",
	"G202": "https://securego.io/docs/rules/g201-g202.html",
	"G304": "https://securego.io/docs/rules/g304.html",
}
