module gitlab.com/gitlab-org/security-products/analyzers/gosec/v2

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/pelletier/go-toml v1.9.3 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.1.2
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.23.0
	gitlab.com/gitlab-org/security-products/analyzers/report/v2 v2.1.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.0.0
	gitlab.com/gitlab-org/security-products/cwe-info-go v1.0.2
	golang.org/x/crypto v0.0.0-20210813211128-0a44fdfbc16e
	golang.org/x/sys v0.0.0-20210816074244-15123e1e1f71 // indirect
	golang.org/x/term v0.0.0-20210615171337-6886f2dfbf5b // indirect
)

go 1.15
